package liquibase.ext.hibernate.database;

/**
* Created with IntelliJ IDEA.
* User: david
* Date: 15/03/13
* Time: 1:42 PM
* To change this template use File | Settings | File Templates.
*/
public enum ConfigType {
    HIBERNATE("hibernate"),
    EJB3("persistence"),
    SPRING("spring");

    private final String prefix;

    ConfigType(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }

    public boolean matches(String url) {
        return url.startsWith(prefix + ":");
    }

    public static ConfigType forUrl(String url) {
        for (ConfigType cfg : values()) {
            if (cfg.matches(url))
                return cfg;
        }
        return null;
    }
}
