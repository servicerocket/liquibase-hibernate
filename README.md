# Introduction

This is a fork of the official [liquibase-hibernate](http://git.org/liquibase/liquibase-hibernate) extension for [liquibase 2.x](http://www.liquibase.org/). It allows you to generate Liquibase Change Log XML files based on comparing your current Hibernate ORM classes to the current database structure.

# What's New

This fork adds the following features to the original:

* Builds against Liquibase 2.0.5 and Java 6/7 APIs.
* Support for loading via Spring configuration files.
* Support for parameters that get passed in via the URL.

# Usage

## Project Structure


## Command Line

*TODO*

## Maven



Ensure the `liquibase-maven-plugin` is listed in your pom.xml:

	<plugin>
		<groupId>org.liquibase</groupId>
		<artifactId>liquibase-maven-plugin</artifactId>
		<version>2.0.5</version>
		<configuration>
			<driver>${liquibase.driverClass}</driver>
		</configuration>
		<dependencies>
			<dependency>
				<groupId>net.customware.liquibase</groupId>
				<artifactId>liquibase-hibernate</artifactId>
				<version>2.0.4</version> 
			</dependency>
		</dependencies>
	</plugin>

On the command line, you can then execute:

	maven liquibase:diff

# Attribution

Original Source:

* [liquibase-hibernate](http://git.org/liquibase/liquibase-hibernate)

Contains code from or inspired by the following other public forks:

* [adrianjgeorge](https://github.com/adrianjgeorge/liquibase-hibernate/commit/497f0af388fce6614deeeaab10aa88c68b671b4a)
* [amplafi](https://github.com/amplafi/liquibase-hibernate/commit/79e763c963d7db856feb3844e220f5d12f8d60e2)


